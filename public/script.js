function getPrices() {
    return {
        prodTypes: [5000, 1390, 10000],
        prodOptions: {
            option1: 0.9,
            option2: 1.5,
            option3: 2
        },
        prodProperties: {
            prop1: 1000,
            prop2: 500
        }
    };
}

function calc() {

    const itemCost = document.getElementById("itemCost");
    const numberOfItems = document.getElementById("numberOfItems");
    if ((/^\d+(?:\.\d+)?$/).test(numberOfItems.value)) {
        document.getElementById("result").innerHTML = "Итоговая цена: "
            + (numberOfItems.value * itemCost.innerHTML);
    } else {
        window.alert("Допустимы только дроби" +
            " или десятичные числа через точку!");
    }
}

function updPriceSelect() {
    let select = document.getElementById("itemType");
    let prices = getPrices();
    let priceIndex = select.selectedIndex;
    let price = prices.prodTypes[priceIndex];

    let radioDiv = document.getElementById("radios");
    radioDiv.style.display = (select.value === "2" ? "block" : "none");

    let checkDiv = document.getElementById("checkboxes");
    checkDiv.style.display = (select.value === "3" ? "block" : "none");

    if (priceIndex === 1) {
        let radios = document.getElementsByName("prodOptions");
        radios.forEach(function (radio) {
            if (radio.checked) {
                let optionPriceMod = prices.prodOptions[radio.value];
                if (optionPriceMod !== undefined) {
                    price *= optionPriceMod;
                }
            }
        });
    } if (priceIndex === 2) {
        let checkbox = document.getElementById("rfd");
        if (checkbox.checked) {
            let propPrice = prices.prodProperties[checkbox.name];
            if (propPrice !== undefined) {
                price += propPrice;
            }
        }
    }
    const itemCost = document.getElementById("itemCost");
    itemCost.innerHTML = price;

    const numberOfItems = document.getElementById("numberOfItems");
    if ((/^\d+(?:\.\d+)?$/).test(numberOfItems.value)) {
        calc();
    }

}


window.addEventListener("DOMContentLoaded", function () {
    updPriceSelect();

    const bt = document.getElementById("button1");
    bt.addEventListener("click", calc);

    let select = document.getElementById("itemType");
    select.addEventListener("change", updPriceSelect);

    let radios = document.getElementsByName("prodOptions");
    radios.forEach(function (radio) { radio.addEventListener("change", updPriceSelect); });
    let checkboxes = document.querySelectorAll("#checkboxes input");
    checkboxes.forEach(function (checkbox) { checkbox.addEventListener("change", updPriceSelect); });

});
